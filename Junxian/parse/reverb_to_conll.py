# This script is used to transform reverb output to conllX-format
# for the maltparser (dependency parser) 
from multiprocessing import Pool
import sys, string

def reverb_to_conll(par):
	reverb = open("reverb_output%s" % par, 'r')
	outfile = open("conll_file%s.txt" % par,'w')
	cnt = 0
	for line in reverb:
		if cnt%500000 == 0:
			print cnt
		sep_line = line.split('\t')
		sentence = sep_line[12]
		postag = sep_line[13]
		words = sentence.split()
		wpostag = postag.split()
		length = len(words)
		for i in range(length):
			outfile.write(str(i+1) + '\t' + words[i] + '\t' + '_'
				+ '\t' + wpostag[i] + '\t' + wpostag[i] + '\t' 
				+ '_' + '\n')
		outfile.write('\n')
		cnt += 1


	reverb.close()
	outfile.close()

if __name__ == '__main__':
    p = Pool()
    parameter = list(string.lowercase)[:10]
    p.map(reverb_to_conll, parameter)