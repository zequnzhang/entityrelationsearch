# This script is used to simplify the reverb_output to its subset where 
# both the subject and object must be tagged entities.

file_in = open('reverb_out_corpus.txt')
file_out = open('simplified_reverb.txt', 'w')
cnt = 0
for line in file_in:
	if cnt % 1000000 == 0:
		print cnt
	sep_line = line.split('\t')
	entity1 = sep_line[2]
	entity2 = sep_line[4]
	if ('_' in entity1) and ('_' in entity2):
		file_out.write(line)
	cnt += 1
file_out.close()