
# This script is used to construct the python dictionary
import cPickle
import relation_class


def split_entity(entity):
	spl = entity.split()
	for obj in spl:
		if '_' in obj:
			obj = obj.split('_')
			obj = obj[-2]
			obj = obj.split('-')
			obj = ' '.join(obj)
			break
	return obj

file_in = open('./reverb_out_corpus.txt','r')
lines = 1
cnt=0
dic={}

for line in file_in:
	if cnt%1000000==0:
		print cnt
	sep_line=line.split('\t')
	normal_relt = sep_line[-2]
	normal_relt = normal_relt.split()
	relt=''
	for j in range(len(normal_relt)-1):
		relt+=normal_relt[j]+'-'
	relt+=normal_relt[-1]+'_'+'Relation'

	entity1 = sep_line[2]
	entity2 = sep_line[4]
	if '_' in entity1 and '_' in entity2:
		entity1 = split_entity(entity1)
		entity2 = split_entity(entity2)
		triple = entity1+'/'+entity2
		confidence = float(sep_line[11])
		ID = cnt
		if dic.has_key(triple):
			dic[triple].append(relation_class.relation(ID, confidence, relt)) 
		else:
			dic[triple]=[relation_class.relation(ID, confidence, relt)]
	cnt += 1
	# if cnt>10000000:
	# 	break

file_in.close()
cPickle.dump (dic, open('dict.p','wb'), -1)
