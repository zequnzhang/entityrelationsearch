from multiprocessing import pool
from bllipparser import RerankingParser
import string

def multi_phrase_parse(s, rrp):
	file_in = open('./reverb_out%s' % s)
	file_out = open('./phrase_out%s' % s)
	for line in file_in:
		sep_line = line.split('\t')
		sentence = sep_line[12]
		file_out.write(rrp.simple_parse(sentence) + '\n')
	file_out.close()

def multi_phrase(arg):
	return multi_phrase_parse(*arg)

if __name__ == '__main__':
    p = Pool()
    parameter = []
    parse = RerankingParser.fetch_and_load('GENIA+PubMed', verbose=True)
    suffix = string.lowercase[0:10]
 	suffix = list(suffix)
 	for ele in suffix:
 		parameter.append((ele, parse))
    p.map(multi_phrase, parameter)
	