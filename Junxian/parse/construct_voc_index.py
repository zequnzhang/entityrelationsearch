import cPickle
import relation_class

file_in = open('./reverb_out_corpus.txt','r')
lines = 1
cnt=0
dic={}

for line in file_in:
	if cnt%1000000 == 0:
		print cnt
	sep_line = line.split('\t')
	sentence = sep_line[12]
	entity1 = sep_line[2]
	entity2 = sep_line[4]
	if '_' in entity1 and '_' in entity2:
		sep_sentence = sentence.split(' ')
		length = len(sep_sentence)
		for i in range(length):
			if dic.has_key(sep_sentence[i]):
				dic[sep_sentence[i]].append(cnt)
			else:
				dic[sep_sentence[i]] = [cnt]
	cnt += 1
	# if cnt>10000000:
	# 	break

file_in.close()
cPickle.dump (dic, open('vocb_dict.p','wb'), -1)
