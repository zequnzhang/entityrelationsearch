file=open('../Data/reverb_output.txt','r')
file_out=open('./parsed_out.txt','w')
a=file.readlines()
result=[]
for line in a:
    sep_line=line.split('\t')
    sentence=sep_line[12]
    start_index=sep_line[7]
    end_index=sep_line[8]
    normal_relt=sep_line[-2]
    normal_relt=normal_relt.split()
    relt=''
    for i in range(len(normal_relt)-1):
        relt+=normal_relt[i]+'-'
    relt+=normal_relt[-1]+'_'+'Relation'
    sep_sentence=sentence.split()
    
    tmp_result=''
    length=len(sep_sentence)
    i=0
    while i<length:
        if (i==int(start_index)):
            tmp_result+=(relt+' ')
            i=int(end_index)
        else:
            if (sep_sentence[i]!=',' and sep_sentence[i]!='.'):
                tmp_result+=(sep_sentence[i]+' ')
            i+=1
    result.append(tmp_result)

for j in range(len(result)):
	file_out.write(result[j]+'\n')
	
file_out.close()