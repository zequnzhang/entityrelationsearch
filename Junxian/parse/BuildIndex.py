from os import system
import re, json, sys
from elasticsearch import Elasticsearch, helpers

#pattern = re.compile("[^a-zA-Z0-9\ \-\_\t\.\,\/\(\)\[\]\&\*\^\$\#\@\~\!\%\?\:\<\>]")
#entity_types = ["Chemical", "Gene", "Species", "Disease", "Proteinmutation", "Genemutation"]
#entity_type_pattern = re.compile("_[Cc]hemical|_[Gg]ene|_[Ss]pecies|_[Dd]isease|_[Pp]roteinmutation|_[Gg]enemutation")
def ps_to_stence(ps):
    sep_ps = ps.split()
    result = []
    for ele in sep_ps:
        if ele[0] != '(':
            result.append(ele[:].strip(')'))
    return ' '.join(result)

def Store(s, phrase):
    global linked
    global actions
    global tokens
    # Content from reverb output
    for i in xrange(len(tokens)):
        tokens[i] = tokens[i].replace("_", "-").split("\t")
    d["sentence"] = [tokens[i][1] for i in xrange(len(tokens))]
    if d["sentence"] != ps_to_stence(phrase).split():
        return False

    d = {}
    terms = s.split("\t")
    # for i in xrange(len(terms)):
    #     terms[i] = terms[i].replace("_", "-")
        #for j in xrange(len(entity_types)):
        #    terms[i] = "".join(terms[i].split("_" + entity_types[j]))
    d["pentity1"] = terms[2].replace('_', '-')
    d["prelation"] = terms[3]
    d["pentity2"] = terms[4].replace('_', '-')
    d["offset"] = [int(terms[w]) for w in xrange(5, 11)]
    d["confidence"] = float(terms[11])
    #d["sentence"] = terms[12]
    #d["postag1"] = terms[13]
    #d["postag2"] = terms[14]
    d["entity1"] = terms[15].replace('_', '-')
    d["relation"] = terms[16]
    d["entity2"] = terms[17].replace('_', '-')
 
        #for w in xrange(entity_types):
        #    tokens[i] = "".join(tokens[i].split("_" + w))
    
    d["postag"] = [tokens[i][3] for i in xrange(len(tokens))]
    d["head"] = [int(tokens[i][6]) for i in xrange(len(tokens))]
    d["role"] = [tokens[i][7].lower() for i in xrange(len(tokens))]
    d["phrase"] = phrase
    # Content from reverb output
    #print " ".join(d["sentence"])
    #print d["entity1"] + "\t" + d["relation"] + "\t" + d["entity2"]
    action = {
               "_index": "research",
               "_type": "corpus",
               "_id": count,
               "_source": d
               }
    actions.append(action)
    return True
    # Previous methods
    #    linked += '{"create":  { "_index": "research", "_type": "corpus", "_id": "' + str(count) + '"}}\n'
    #    linked += json.dumps(d) + "\n"
    #    res = es.index(index="research", doc_type='corpus', id=count, body=d)
    #    print(res['created'])
    #    system("curl -XPUT 'http://localhost:9200/research/corpus/"+str(count)+"' -d '" + json.dumps(d) + "'")
es = Elasticsearch("http://user:junxian@192.17.58.145:9200")
suffix = sys.argv[1]
count = 0
actions = []
conllx = open("./conll_out_%s.txt" % suffix, "r")
tokens = []
current = ""
file2 = open('phrase_structure_%s.txt' % suffix)
with open("./split_reverb_%s.txt" % suffix) as file:
    for (line, phrase) in zip(file, file2):
        # Process the line for terminal character
        # while (line.endswith("\r") or line.endswith("\n")): line = line[:-1]
        # if (line == ""): continue
        line = line.strip()
        phrase = phrase.strip()
        # Process the conllx content
                # Clear conllx content
        tokens = []
        current = conllx.readline()
        while current != "\n" and current != "\r\n":
            current = current.strip()
            tokens.append(current)
            current = conllx.readline()
        #
        flag = Store(line, phrase)
        if flag:
            count += 1
        else:
            continue


        # Cache and print
        if (count % 1000 == 0):
            sys.stdout.write('\r')
            sys.stdout.write("[%-50s] % 3d%%      Processed to line %d" % ('='*(count/2800000), count/1400000, count))
            sys.stdout.flush()
        if (count % 10000 == 0):
            helpers.bulk(es, actions)
            actions = []
            #if (count == 80860000): break