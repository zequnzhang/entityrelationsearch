from collections import Counter
from bllipparser import RerankingParser
class PatternLearner(object):
	"""docstring for PatternLearner"""
	def __init__(self):
		super(PatternLearner, self).__init__()
		self.rrp = RerankingParser.fetch_and_load('GENIA+PubMed', verbose=True)
	def context_pos(sentence):
		pos = []
		sep_sentence = sentence.split(' ')
		for i in range(len(sep_sentence)):
			if sep_sentence[i].count('@') == 2:
				sep_sentence[i] = sep_sentence[i].split('@')[2]
				for j in range(i+1, len(sep_sentence)):
					if sep_sentence[j].count('@') == 1:
						sep_sentence[j] = sep_sentence[j].split('@')[0]
						pos.append((i, j))

	def delLabel(sentence):
		sep_sentence = sentence.split(' ')
		for i in range(len(sep_sentence)):
			if sep_sentence[i].count('@') == 2:
				sep_sentence[i] = sep_sentence[i].split('@')[2]
				for j in range(i+1, len(sep_sentence)):
					if sep_sentence[j].count('@') == 1:
						sep_sentence[j] = sep_sentence[j].split('@')[0]
		return sep_sentence

	def LearnPattern(sentenceSet, posSet, offsetSet):
		pattern = Counter()
		for i in range(len(sentence)):
			sentence = sentenceSet[i]
			pos = posSet[i]
			offset = offsetSet[i]
			for posele in pos:
				instance = extract_pattern(sentence, posele, offset)
				if instance == 1:
					print "Extraction Error"
					return
				pattern[instance] += 1
		return pattern

	def extract_pattern(sentence, pos, offset):
		parse_out = self.rrp.simple_parse(sentence)
		pares_out = parse_phrase(parse_out)
		structure = find_min(parse_out, pos)
		if pos[0] >= offset[5]:
			if pos[0] == offset[5]: 
				return 'arg1 | rel | arg2 |' + structure
			elif pos]0] - offset[5] <= 5:
				return 'arg1 | rel | arg2 | * |' + structure
			else:
				return 'arg1 | rel | arg2 | ** |' + structure
		elif pos[1] < offset[0]:
			if pos[1] == offset[0] - 1:
				return structure + ' | arg1 | rel | arg2'
			elif offset[0] - pos[1] < 5:
				return structure + ' | * | arg1 | rel | arg2'
			else:
				return structure + ' | ** | arg1 | rel | arg2'
		else:
			return 1

	def find_min(parse_out, pos):
		flag = True
		result = ''
		start = pos[0]
		while flag:
			for ele in reversed(parse_out[start]):
				if ele[1] == pos[1]:
					flag = False
					result += ' | ' + ele[0]
					break
				elif ele[1] < pos[1]:
					result += ' | ' + ele[0]
					start = ele[1] + 1
		return result


file_in = open('context_sample.txt')
sentenceSet = []
posSet = []
offsetSet = []
pl = PatternLearner()
for line in file_in:
	if line != '\n':
		if '@' not in line:
			break
		else:
			sep_line = line.split('\t')
			sentence = sep_line[1]
			triple = sep_line[0]
			postmp = pl.context_pos(sentence)
			sentencetmp = pl.delLabel(sentence)
			offsettmp = triple.strip(')').split(',')[-6:]
			offsettmp = map(int, offsettmp)
			sentenceSet.append(sentencetmp)
			posSet.append(postmp)
			offsetSet.append(offsettmp)
pattern = pl.LearnPattern(sentenceSet, posSet, offsetSet)

