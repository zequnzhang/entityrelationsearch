file_in = open('reverb_out_corpus.txt')
file_out = open('context_sample.txt')
cnt = 0
for line in file_in:
	if cnt % 10000 == 0:
		sep_line = line.split('\t')
		sentence = sep_line[12]
		file_out.write(sentence + '\n')
	if cnt == 10000000:
		break

file_in.close()
file_out.close()
