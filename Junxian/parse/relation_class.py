class relation(object):
	"""docstring for relation"""
	def __init__(self, id, confidence, name):
		super(relation, self).__init__()
		self.id = id
		self.confidence = confidence
		self.name = name

class vocab(object):
	"""docstring for vocab"""
	def __init__(self, id, position):
		super(vocab, self).__init__()
		self.id = id
		self.position = position
						