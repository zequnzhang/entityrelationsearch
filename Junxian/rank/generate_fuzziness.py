from collections import Counter
import cPickle

tag = ["Chemical", "Gene", "Species", "Disease"]
fuzziness = {}
global_entity_pair = Counter()
file = open('simplified_reverb.txt')
cnt = 0
for line in file:
	if cnt % 100000 == 0:
		print cnt
	cnt += 1
	sep_line = line.split('\t')
	pentity1 = sep_line[2].split()
	pentity2 = sep_line[4].split()
	relation = sep_line[-2]
	flag = True
	for word in pentity1:
		split_word = word.split('_')
		if split_word[-1] in tag:
			tmp1 = word
			flag = False
			break
	if flag:
		continue

	flag = True
	for word in pentity2:
		split_word = word.split('_')
		if split_word[-1] in tag:
			tmp2 = word
			flag = False
			break

	if flag:
		continue

	entity_pair = tmp1 + '@' + tmp2
	global_entity_pair[entity_pair] += 1
	if fuzziness.has_key(relation):
		fuzziness[relation][entity_pair] += 1
	else:
		fuzziness[relation] = Counter()
		fuzziness[relation][entity_pair] += 1

length = len(global_entity_pair)
length = float(length)
fuzzy_pr = {}
for key in fuzziness:
	fuzzy_pr[key] = len(fuzziness[key])/length

cPickle.dump(fuzzy_pr, open('fuzzy_pr.p','wb'))

