# Entity Relation Search
This is python implementation of a demo system for Entity Relation Search.  
Contact: junxianh2@gmail.com

************

# Usage
```python
python ./UI/main.py
```
This is the simplest and fastest way to run the demo, but it cannot perform clustering due to lack of relation embeddings.  
Users can activate the clustering function by specifying `-model` and the embedding file, loading the embedding file might take several minutes.