import sys, word2vec
rl = ['advcl', 'advmod', 'amod', 'ccomp', 'neg', 'nn', 'prep', 'rcmod', 'tmod', 'xcomp']
patternSet = [(3, 0, 0, ['PP']), (0, 0, 0, ['PP', ',']), (3, 1, 1, [',', 'PP']), (3, 0, 1, ['IN', 'NP']), (3, 0, 0, ['SBAR'])]

if len(sys.argv) != 1:
	if sys.argv[1] == '-model':
		print "loading word embedding"
		model = word2vec.load(sys.argv[2])
		print "complete load wordembedding"
else:
	model = ''

import Tkinter as tk
from EntityRelationRank import EntityRelationRank
root = tk.Tk()
root.geometry("1100x800")
app = EntityRelationRank(root, patternSet, rl, model)
root.mainloop()