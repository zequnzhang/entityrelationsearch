from collections import Counter
from affiliation import *

class PatternLearner(object):
    """docstring for PatternLearner"""
    def __init__(self, patternset = [], rrp = []):
        super(PatternLearner, self).__init__()
        self.patternset = patternset
        self.rrp = rrp
    # Return the position of labeled context in a sentence
    def context_pos(self, sentence):
        pos = []
        sep_sentence = sentence.split(' ')
        for i in range(len(sep_sentence)):
            if sep_sentence[i].count('@') == 2:
                sep_sentence[i] = sep_sentence[i].split('@')[2]
                for j in range(i+1, len(sep_sentence)):
                    if sep_sentence[j].count('@') == 1:
                        sep_sentence[j] = sep_sentence[j].split('@')[0]
                        pos.append((i, j))
        return pos

    # Return the original sentence from labeled one
    def delLabel(self, sentence):
        sep_sentence = sentence.split(' ')
        for i in range(len(sep_sentence)):
            if sep_sentence[i].count('@') == 2:
                sep_sentence[i] = sep_sentence[i].split('@')[2]
                for j in range(i+1, len(sep_sentence)):
                    if sep_sentence[j].count('@') == 1:
                        sep_sentence[j] = sep_sentence[j].split('@')[0]
        return sep_sentence

    # Learn the pattern
    def LearnPattern(self, sentenceSet, posSet, offsetSet):
        pattern = Counter()
        for i in range(len(sentenceSet)):
            sentence = sentenceSet[i]
            pos = posSet[i]
            offset = offsetSet[i]
            for posele in pos:
                instance = self.extract_pattern(sentence, posele, offset)
                if instance == 1:
                    print "Extraction Error"
                    return
                pattern[instance] += 1
        return pattern

    # Extract pattern from a sentence
    def extract_pattern(self, sentence, pos, offset):
        parse_out = self.rrp.simple_parse(sentence)
        parse_out = parse_phrase(parse_out)
        structure = self.find_min(parse_out, pos)
        if pos[0] >= offset[5]:
            tmp = self.find_min(parse_out, (offset[5], pos[0] - 1))
            return 'arg1 | rel | arg2 | ' + tmp + '* | ' + structure + '* | '
        elif pos[0] >= offset[3] and pos[1] < offset[4]:
            tmp1 = self.find_min(parse_out, (offset[3], pos[0] - 1))
            tmp2 = self.find_min(parse_out, (pos[1] + 1, offset[4] - 1))
            return 'arg1 | rel | ' + tmp1 + '* | ' + structure + '* | ' + tmp2 + 'arg2 | '
        elif pos[0] >= offset[1] and pos[1] < offset[2]:
            tmp1 = self.find_min(parse_out, (offset[1], pos[0 - 1]))
            tmp2 = self.find_min(parse_out, (pos[1] + 1, offset[2] - 1))
            return 'arg1 | ' + tmp1 + '* | ' + structure + '* | ' + tmp2 + 'rel | arg2 | '
        elif pos[1] < offset[0]:
            tmp = self.find_min(parse_out, (pos[1] + 1, offset[0] - 1))
            return '* | ' + structure + '* | ' + tmp + 'arg1 | rel | arg2 | '
        else:
            print pos
            print offset
            return 1
    # Find the minimal structure for any part in one sentences
    def find_min(self, parse_out, pos):
        if pos[0] > pos[1]:
            return ''
        flag = True
        result = ''
        start = pos[0]
        while flag:
            for ele in reversed(parse_out[start]):
                if ele[1] == pos[1]:
                    flag = False
                    result += ele[0] + ' | '
                    break
                elif ele[1] < pos[1]:
                    result += ele[0] + ' | '
                    start = ele[1] + 1
                    break
        return result
    
    # Match a pattern for a sentence and return the context position (return 1 without context)
    # pattern is expressed in the form of (position, start, end, [pattern]), eg. (3, 1, 1, [',', 'PP'])
    def match_pattern(self, pattern, phrase_out, inverse_parse_out, offset):
        start_list = [offset[0] - 1, offset[1], offset[3], offset[5]]
        upperbound_list = [0, offset[2], offset[4], len(phrase_out)-1]
        start = start_list[pattern[0]]
        upperbound = upperbound_list[pattern[0]]
        structure = pattern[3]
        context_start = pattern[1]
        context_end = pattern[2]
        context_pos = []
        if start == offset[0] - 1:
            if start < 0:
                return 1
            for ele in reversed(structure):
                if start < 0:
                    return 1
                flag = False
                for role in inverse_parse_out[start]:
                    if role[0] == ele:
                        flag = True
                        context_pos.append((role[1], start))
                        start = role[1] - 1
                        break
                if not flag:
                    return 1
            context_pos = context_pos[::-1]
            return (context_pos[context_start][0], context_pos[context_end][1])
        else:
            for ele in structure:
                if start >= upperbound:
                    return 1
                flag = False
                for role in phrase_out[start]:
                    if role[0] == ele:
                        flag = True
                        context_pos.append((start, role[1]))
                        start = role[1] + 1
                if not flag:
                    return 1
            return (context_pos[context_start][0], context_pos[context_end][1])
    #This is to get context position from a sentence given a pattern set
    def get_context(self, ps, offset):
        phrase_out = parse_phrase(ps)
        inverse_parse_out = inverse_parse(ps)
        for pattern in self.patternset:
            tmp = self.match_pattern(pattern, phrase_out, inverse_parse_out, offset)
            if tmp != 1:
                return tmp
        return 1