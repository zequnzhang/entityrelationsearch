from collections import Counter
from PatternLearner import PatternLearner
from affiliation import *
# RelationList = ['advcl', 'advmod', 'amod', 'ccomp', 'neg', 'nn', 'prep', 'rcmod', 'tmod', 'xcomp']
# adv = {"time":['when', 'before', 'after', 'since', 'while', 'as long as', 'till', 'untill']}
# adv['condition'] = ['if', 'unless', 'lest']
# adv['purpose'] = ['in order to', 'so that', 'in order that']
# adv['reason'] = ['because', 'since', 'given']
# adv['concession'] = ['although', 'though']
# adv['place'] = ['where', 'wherever', 'anywhere', 'everywhere']

class Relation(object):
    """docstring for Relation"""
    def __init__(self, score = 0, pl = PatternLearner(), relationlist=[]):
        super(Relation, self).__init__()
        self.score = score
        self.modifier1 = Counter()        #key: "head dependent"; value: count (score)
        self.modifier2 = Counter()
        self.context = []
        self.pl = pl
        self.RelationList = relationlist
        self.ct_entity_cnt = Counter()
        self.ct_entity_st = {}
        self.ct_entity = {}
    # This is for entity modifier
    def update_modifier(self, arg1, arg2, head, role, offset, postag, sentence):
        index1 = self.get_index(arg1, offset[0:2], sentence)
        index2 = self.get_index(arg2, offset[4:], sentence)
        dependent1 = self.get_dependent(index1, head, offset[2:4])
        dependent2 = self.get_dependent(index2, head, offset[2:4])
        # indexr = self.get_keyword(offset, postag)
#         dependentr = self.get_dependentr(head, offset[2:4])
        modifier = [self.modifier1, self.modifier2]
        indexSet = [index1, index2]
        arg = [arg1, arg2]
#         depSet = [dependent1, dependent2, dependentr]
        depSet = [dependent1, dependent2]
        triple = []
        #Remember that head[] starts from 1, and 0 represents ROOT.
        for i in range(2):
            index = indexSet[i]
            if head[index] != 0:
                if ((head[index]-1) not in range(offset[2],offset[3])) and (role[index] in self.RelationList):
                    modifier[i][sentence[head[index]-1] + '@' + arg[i]] += 1
        for i in range(2):
            for ele in depSet[i]:
                if role[ele] in self.RelationList:
                    modifier[i][arg[i] + '@' + sentence[ele]] += 1
#                     if role[ele] == 'prep':
#                         tmp = self.get_dependent(ele, head, offset[2:4])
#                         for tmpele in tmp:
#                             if i != 2:
#                                 triple.append(arg[i] + '@' + ' '.join(sentence[ele : ele+7]))
#                             else:
#                                 triple.append(relation + '@' + ' '.join(sentence[ele : ele+7]))
#                     else:
#                         if i != 2:
#                             triple.append(arg[i] + '@' + sentence[ele])
#                         else:
#                             triple.append(relation + '@' + sentence[ele])
#         for triplele in triple:
#             self.modifier[triplele] += 1
    # This is recognizing context in terms of adverbial clause
    def update_context_adverbial(self, sentence, ps, offset):
        index = -1
        for key in adv:
            for ele in adv[key]:
                index = -1
                if len(ele.split()) == 1:
                    for i in range(len(sentence)):
                        if ele == sentence[i].lower():
                            index = i
                            break
                else:
                    tmp = ele.split()[-1]
                    tmp2 = ele.split()[-2]
                    if len(sentence) == 1:
                        print "error"
                    for i in range(1,len(sentence)):
                        if sentence[i].lower() == tmp and sentence[i-1].lower() == tmp2:
                            index = i
                            break
#                 if index == -1:
#                     print ele
#                     print sentence
#                     break
                if index != -1 and index != len(sentence)-1:
                    parse_out = parse_phrase(ps)
                    for phrase in parse_out[index+1]:
                        if phrase[1] < offset[0] or index >= offset[5]:
                            if phrase[0] == 'S':
                                self.context.append(key + '@' + ' '.join(sentence[index:phrase[1]+1]))
    # This is recognizing context in terms of constituency parsing and pattern match   
    def update_context_constt(self, sentence, ps, offset):
        result = self.pl.get_context(ps, offset) 
        if result != 1:
            context = ' '.join(sentence[result[0]:result[1]+1])
            join_sentence = ' '.join(sentence)
            for i in range(result[0], result[1] + 1):
                tmp = sentence[i]
                if '-' in tmp:
                    tmp2 = sentence[i].split('-')
                    if tmp2[-1] in ["Chemical", "Gene", "Species", "Disease", "Proteinmutation", "Genemutation"]:
                        self.ct_entity_cnt[tmp] += 1
                        if self.ct_entity.has_key(tmp):
                            self.ct_entity[tmp].append(context)
                            self.ct_entity_st[tmp].append(join_sentence)
                        else:
                            self.ct_entity[tmp] = [context]
                            self.ct_entity_st[tmp] = [join_sentence]
            self.context.append(context)
    def sort_modifier(self, order = True):
        tmp = self.modifier.most_common(10)
        return tmp
    def get_index(self, word, offset, sentence):
        flag = False
        for i in range(offset[0], offset[1]):
            if word.lower() in sentence[i].lower():
                flag = True
                return i
        print word
        print sentence
    #eliminate the head/dependent in the other parts of the tuple
    def get_dependent(self, index, head, exception):
        result = []
        for i in range(len(head)):
            if (head[i]-1 == index) and (i not in range(exception[0], exception[1])):
                result.append(i)
        return result
    def get_dependentr(self, head, offset):
        result = []
        for i in range(len(head)):
            if ((head[i]-1) in range(offset[0],offset[1])) and (i not in range(offset[0], offset[1])):
                result.append(i)
        return result

class RelationSet(object):
    """docstring for RelationSet"""
    def __init__(self, patternset, relationlist):
        super(RelationSet, self).__init__()
        self.storage = {}
        self.patternset = patternset
        self.relationlist = relationlist
    def update_relation(self, rel, confidence):
        if self.storage.has_key(rel):
            self.storage[rel].score += confidence
        else:
            self.storage[rel] = Relation(score = confidence, pl = PatternLearner(patternset = self.patternset), relationlist = self.relationlist)
    def sort_score(self, order = True):
        tmp = sorted(self.storage.items(), key = lambda ele: ele[1].score, reverse = order)
        result = []
        for triple in tmp:
            result.append(triple[0])
        return result
    def get_scoreitems(self):
        result = []
        for ele in self.storage:
            result.append((ele, self.storage[ele].score))
        return result
    def get_scores(self):
        result = []
        for ele in self.storage:
            result.append(self.storage[ele].score)
        return result
    def update_modifier(self, arg1, arg2, relation, head, role, offset, postag, sentence):
        self.storage[relation].update_modifier(arg1, arg2, head, role, offset, postag, sentence)
    def update_context_adverbial(self, relation, sentence, ps, offset):
        self.storage[relation].update_context_adverbial(sentence, ps, offset)
    def update_context_constt(self, relation, sentence, ps, offset):
        self.storage[relation].update_context_constt(sentence, ps, offset)
    def sort_context(self):
        for ele in self.storage:
            self.storage[ele].sort_context()
    def new_score(self, prior, norm, info):
        for ele in self.storage:
            self.storage[ele].score = (self.storage[ele].score + prior[ele] * norm) * info[ele]


