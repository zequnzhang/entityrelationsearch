# parse the constituency structure
# the ith element contains all the portions which ends at i and its start position
# eg. inverse_parse("(PP (IN in) (NP (DT the) (NNS patients)))")
# [[('IN', 0)], [('DT', 1)], [('NNS', 2), ('NP', 1), ('PP', 0)]]
def inverse_parse(input):
    sep_input = input.split()
    word_cnt = 0
    result = [[]]
    stack = []
    for ele in sep_input:
        if ele[0] == '(':
            stack.append((ele[1:],word_cnt))
        else:
            for char in ele[::-1]:
                if char == ')':
                    tmp = stack.pop()
                    result[word_cnt].append(tmp) 
            word_cnt += 1
            result.append([])
    result.pop()
    return result
# parse the constituency structure
# the ith element contains all the portions which starts at i and its end position
# eg. parse_phrase("(PP (IN in) (NP (DT the) (NNS patients)))")
# [[('IN', 0), ('PP', 2)], [('DT', 1), ('NP', 2)], [('NNS', 2)]]
def parse_phrase(input):
    sep_input = input.split()
    word_cnt = 0
    result = [[]]
    stack = []
    for ele in sep_input:
        if ele[0] == '(':
            stack.append((ele[1:],word_cnt))
        else:
            for char in ele[::-1]:
                if char == ')':
                    tmp = stack.pop()
                    result[tmp[1]].append((tmp[0],word_cnt)) 
            word_cnt += 1
            result.append([])
    result.pop()
    return result

# transform the phrase structure to string sentences
def ps_to_stence(ps):
    sep_ps = ps.split()
    result = []
    for ele in sep_ps:
        if ele[0] != '(':
            result.append(ele[:].strip(')'))
    return ' '.join(result)



def splitRelation(relation):
    out = relation.split('_')
    out = out[-2]
    out = out.split('-')
    out = ' '.join(out)
    return out

def joinRelation(relation):
    out = relation.split(' ')
    out = '-'.join(out)
    out += '_Relation'
    return out