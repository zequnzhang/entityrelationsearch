from sklearn.cluster import KMeans
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.tokenize import word_tokenize
# from nltk.corpus import wordnet as wn
from gensim.models import Word2Vec


# TODO:change cluster #
cluster_num = 3


lemma = nltk.wordnet.WordNetLemmatizer()


def preprocess(corpus):

    corpus_new  = []
    corpus_word2vec = []

    for rp in corpus:
        words = word_tokenize(rp)
        words_new = [lemma.lemmatize(word) for word in words]
        corpus_new.append(' '.join(words_new))
        corpus_word2vec.append(words_new)
    return (corpus_new,corpus_word2vec)


def featureVect(corpus):
    ngram_vectorizer = CountVectorizer(ngram_range=(1,10),stop_words="english")
    X_2 = ngram_vectorizer.fit_transform(corpus).toarray()

    vectorizer = TfidfVectorizer(ngram_range=(1,10),stop_words="english")
    X_2_DFIDF=vectorizer.fit_transform(corpus).toarray()

    X = np.multiply(X_2,X_2_DFIDF)

    return X

def word2vecOption(corpus_word2vec):
    # Size: word feature dimensions, window: chunk span size, min_count: min freq of each indexed words
    model = Word2Vec(corpus_word2vec,size=20, window=5, min_count=1)
    vocab = model.index2word
    word_features = model.syn0

    word_matrix = []
    for rp in corpus_word2vec:
        features = np.zeros((len(vocab)))
        for word in rp:
            index = vocab.index(word)
            features[index] = 1
        word_matrix.append(features)

    X = np.dot(np.array(word_matrix),word_features)
    return X

def k_means(frequency_vectors):
    X = np.array(frequency_vectors)
    est = KMeans(n_clusters=cluster_num)
    est.fit(X)
    labels = est.labels_
    print(labels)


def main():
    # corpus = list of relation phrases
    # TODO: change to real RPs
    RPs = ['is treatement of','to treat','cause','reduce','treats']





    (corpus_new,corpus_word2vec) = preprocess(RPs)

    # TODO: swtich on two methods

    # Method1: IFIDF * Freq
    # frequency_vectors = featureVect(corpus_new)

    #Method2: Word2Vec
    frequency_vectors = word2vecOption(corpus_word2vec)


    # TODO: add entity type pair info into vectors as well

    # Add word2vec can remvove
    k_means(frequency_vectors)


if __name__ == "__main__":
    main()