from flask.ext.cors import CORS
from elasticsearch import Elasticsearch
from nltk.corpus import wordnet
from datetime import datetime
from flask import Flask, request,json
import string
import cPickle
import time

app = Flask(__name__)
CORS(app)

cache_entity_contexts_ram = {}
cache_suggest_keywords = {}
cache_ranked_rps = {}
cache_verb_syn_set_ram = {}

@app.route('/')
def hello():
    return 'Welcome to Entity Research!'


def init_postag_map():
    postag_map = {}
    
    VTagList = ["VB", "VBD", "VBG", "VBN","VBP","VBZ","RB","RBR","RBS",   "WRB"]
    ADJTagList = ["JJ","JJR","JJS"]
    
    WTagList = ["PRP","PRP$","WP","WP$","PDT","WDT"]
    NTagList = ["NN","NNS","NNP","NNPS"]  
    
    #WTagList = ["NN","NNS","NNP","NNPS","JJ","JJR","JJS","RB","RBR","RBS","PRP","PRP$","WP","WP$","PDT","WDT"]
    PTagList = ["IN","RP"]
    
    for tag in VTagList:    postag_map[tag] = 'V'
    for tag in ADJTagList:  postag_map[tag] = 'ADJ' 
    
    for tag in WTagList:    postag_map[tag] = 'W'
    for tag in PTagList:    postag_map[tag] = 'P'
    
    for tag in NTagList:    postag_map[tag] = 'N'
    
    return postag_map

def init_pattern_score_map():
    pattern_score_map = {}
    pattern_txt = open("pattern_nsc_DC2.txt")
    lines = pattern_txt.readlines()
    for line in lines:
        tmp = line.strip().split('\t')
        try:
            pattern_score_map[tmp[0]] = float(tmp[1])
        except:
            print "error in init_pattern_score_map()"
    return pattern_score_map 
    
def parse_query(request):
    query_entities = []
    
    ent_tag_list = ["[DISEASE","[GENE","[SPECIES","[CHEMICAL","[MUTATION"]
    
    try:
        
        
        entity_1 = request.args["entity1"]
        entity_2 = request.args["entity2"]
        
        query_entities.append( str(entity_1) )
        query_entities.append( str(entity_2) )
        
        
        keywords = request.args["keywords"].split(',')
        
        for keyword in keywords:
            if len(keyword)>0 and "." not in keyword:
                
                if keyword[0] == "[" and keyword[ :keyword.find(']') ]  in ent_tag_list:
                    query_entities.append( keyword[ keyword.find(']')+1: ] )
                    
                else:
                    query_entities.append( str(keyword) )
        
                
    except:
        print "error in parse_query(), query=", query_dict
    
   
    
    print query_entities
    return query_entities
    
    
def load_context_vectors_lists( es, query_entities ):
    
    global cache_entity_contexts_ram
    
    parallel_context_vectors_lists = []
    
    
    for query_entity in query_entities:
        
        if query_entity in cache_entity_contexts_ram:
            parallel_context_vectors_lists.append( list(cache_entity_contexts_ram[query_entity]) )
            continue
            
        
        id_list = []
        context_vector_list = []
        position_list = []
        
        try:
            result = es.search(index='context-newest', size=2147483646, body={"query": {"match": {"ent_name": query_entity}}})
        except:
            result = es.search(index='context-newest', size=99999, body={"query": {"match": {"ent_name": query_entity}}})
            
        
        print len(result['hits']['hits'])
        
        for hit in result['hits']['hits']:
            if hit["_source"]["ent_name"]!=query_entity:
                continue
                
            ID = hit["_source"]["ID"]
            context_vector = hit["_source"]["tags"]
            position_in_vector = hit["_source"]["pos"]
        
            id_list.append( ID )
            context_vector_list.append( context_vector )
            position_list.append( position_in_vector )
      
    
        
        #print("--- %s seconds -rank--" % (time.time() - start_time))
        parallel_context_vectors_lists.append( sorted(zip(id_list, position_list, context_vector_list)) )
        
        cache_entity_contexts_ram[ query_entity ] = list( parallel_context_vectors_lists[-1] )
        
        assert len(parallel_context_vectors_lists[-1]) > 0
        
    return parallel_context_vectors_lists



def found_intersecting_context( parallel_indices ):
    
    
    found_intersecting_context_vector = False
    
    while not found_intersecting_context_vector:
        
        for index in xrange(len(parallel_indices)):
            if len(parallel_indices[index]) == 0:
                return False
            
        moved_pointer = False
        for index_i in xrange(len(parallel_indices)):
            for index_j in range(len(parallel_indices)):  
                
                if index_i != index_j:
                    while len(parallel_indices[index_i]) > 0 and parallel_indices[index_i][0][0] < parallel_indices[index_j][0][0]:
                        del parallel_indices[index_i][0]
                        moved_pointer = True
                    if len(parallel_indices[index_i]) == 0:
                        return False
        
        if not moved_pointer:
            found_intersecting_context_vector = True
    
    
    return True
        
    
                
def init_context_lists( context_vector, entity_positions, postag_map ):
    word_list = [] 
    tag_list = []
    phrase_link = [] 
   
    for word in context_vector:
        
        tmp = word.split("@@")
        word_list.append( tmp[0] )
        
       
        if len(tag_list) in entity_positions:
            
            word_list[-1] = tmp[0][ tmp[0].find("]")+1: ]
            tag_list.append( "E" )
            
        elif '[' == tmp[0][0]:
            
            tag_list.append( "E" )
            
        elif tmp[1] in postag_map:
            
            tag_list.append( postag_map[tmp[1]] ) 
        else:
         
            tag_list.append( tmp[1] )
        
        
        phrase_link.append( len(phrase_link) )
       
    return word_list, tag_list, phrase_link
       
def extract_verb_phrases (word_list, tag_list, phrase_link):
    
    tail = 0
    
    while tail < len(tag_list):
        head = tail
        if tag_list[tail] =="V":
            while head+1 < len(tag_list) and tag_list[ head+1 ] in ["V","W","N","P","ADJ"]:
                head += 1
            while head- 1>= tail and tag_list[ head ] in ["W","N","ADJ"]:
                head -= 1
                
            key_verb_position = head
            
            while tag_list[ key_verb_position ] != "V":
                key_verb_position -= 1
            
            tag_list[tail] = "VP"
            
            phrase_link [tail] = head
            word_list [tail] = " ".join( word_list[ tail: head+1 ] ) + "|" + word_list [ key_verb_position ]
        
            for ptr in xrange(tail+1, head+1):
                phrase_link [ptr] = -1
        
        tail =head+1
        
    
    return word_list, tag_list, phrase_link           
 
def extract_entity_modifiers (word_list, tag_list, phrase_link):
    
    for ptr in xrange( len(tag_list) ):
        if tag_list [ptr] == 'E':
            if (ptr+1) <len( tag_list ):
                if tag_list[ ptr+1 ] =='N':
                    tag_list[ ptr+1 ] = 'EM'
                    
            front = ptr
            
            while (front>0) and tag_list[front-1] in ["ADJ","$","CD","N"]:
                front -= 1
                tag_list[front] ="EM"
                
                phrase_link[front] = ptr -1
                
                
                if tag_list[front] in ["ADJ","$"]:
                    break
                    
            for tmp in xrange( front+1, ptr):
                phrase_link [tmp] = -1
                
            if front< ptr:
                word_list [front] = " ".join(  word_list [ front:ptr ] )
                
    return word_list, tag_list, phrase_link


def extract_pattern (word_list, tag_list, phrase_link):
    pos_pattern = ""
    
    return pos_pattern
     
def prepare_output (word_list, tag_list, phrase_link, entity_positions, pmid_string):
    
    
    single_context_output_list = []
    
    ptr = 0
    while ptr < len (word_list):
        
        phrase_dict = {}
        
        if tag_list[ptr] == "VP":
            phrase_dict ["word"] = word_list[ptr].split('|')[0]
        else:
            phrase_dict ["word"] = word_list[ptr] if tag_list[ptr] !="E" else word_list[ptr][ word_list[ptr].find(']')+1: ]
        
        if tag_list[ptr] == "EM":
            phrase_dict ["type"] = "em"
            
        elif tag_list[ptr] == "VP":
            phrase_dict ["type"] = "vp"
        
        elif tag_list[ptr] == "E":
            phrase_dict ["type"] = "entity" if ptr in entity_positions[:2] else "keyword"
        
        else:
            phrase_dict ["type"] = "none"
             
        ptr = phrase_link[ptr] + 1
        single_context_output_list.append( phrase_dict )
        
        
    single_tuple_output_dict = {}
    
    single_tuple_output_dict['tokens'] = single_context_output_list
    single_tuple_output_dict['link'] = "http://www.ncbi.nlm.nih.gov/pubmed/" + pmid_string
    
    return single_tuple_output_dict


def record_output (word_list, tag_list, phrase_link, single_tuple_output_dict, entity_positions, verb_phrases_score, verb_phrase_output_dict, query_entities, contextual_entity_dict, synonym_dict, cache_synonym_sets ):
    global cache_verb_syn_set_ram
    ptr = max( min( entity_positions[:2] ) +1, 0)
    ptr_end = min( max( entity_positions[:2] ) -1, len(tag_list) -1)
    
    while ptr <= ptr_end:
        
        if phrase_link[ptr] == -1:
            ptr += 1
            
        else:
            if tag_list[ ptr ] == "VP":
                
                this_verb = word_list [ptr] .split('|') [0]
                
                key_verb = word_list [ptr] .split('|') [1]
                
                if not this_verb in synonym_dict:
                        
                    this_verb_syn_set = set()
                    
                    if key_verb in cache_verb_syn_set_ram:
                        this_verb_syn_set = set(cache_verb_syn_set_ram [key_verb] )
                    
                    else:
                        for syn in wordnet.synsets( key_verb ):
                            for tmp in syn.lemmas():
                                this_verb_syn_set.add( tmp.name() )
                                
                        cache_verb_syn_set_ram [key_verb] = set( this_verb_syn_set )
                    
                    find_synonym = False
                    for verb in synonym_dict.keys():
                        
                        if synonym_dict[ verb ] != verb:
                            continue
                        
                        cached_set = cache_synonym_sets [ verb ]
                                
                        if ( len(this_verb_syn_set.union(cached_set) )*100 < ( len(this_verb_syn_set) + len(cached_set))*75):
                            find_synonym = True
                            synonym_dict[ this_verb ] = verb
                            break
                            
                    if not find_synonym:
                        synonym_dict [ this_verb ] = this_verb
                        cache_synonym_sets [ this_verb ] = this_verb_syn_set
                        
                        
                this_verb = synonym_dict [ this_verb ]
                
                if this_verb not in verb_phrases_score:
                    
                    verb_phrase_output_dict[ this_verb ] = []
                    verb_phrases_score[ this_verb ] = 1 # deleted pattern score
                    
                else:
                    verb_phrases_score[ this_verb ] += 1 #patternScoreMap[finalPatternStr]
                
                verb_phrase_output_dict[ this_verb ].append( single_tuple_output_dict )
                
            elif tag_list[ ptr ] == "E":
                if ptr not in entity_positions:
                    
                    contextual_entity_dict[ word_list [ptr]  ] = contextual_entity_dict.get( word_list [ptr] , 0 ) + 1
                    
            ptr = phrase_link[ptr] + 1 


def low_quality_result( word_list, tag_list, phrase_link, entity_positions ):
    
    phrase_count = 0
    ptr = min( entity_positions[:2] ) + 1
    ptr_end = max( entity_positions[:2]) -1
    
    while ptr <= ptr_end:
        phrase_count +=1
        ptr = phrase_link[ ptr ] +1
        
        if phrase_count > 5:
            return True
      
    
    
    return False
    
    


def rank_relations( parallel_indices, postag_map, query_entities ):
    
    verb_phrases_score = {}
    verb_phrase_output_dict = {}
    
    contextual_entity_dict = {}
    
    synonym_dict = {}
    cache_synonym_sets ={}
    
    while found_intersecting_context( parallel_indices ):
        
        context_vector = parallel_indices[0][0][2].split("##")[:-1]   # all context
        pmid_string = parallel_indices[0][0][0].split("|")[0]
        
        entity_positions = []    
        for ptr in xrange(len(parallel_indices)):
            entity_positions.append(  parallel_indices[ptr][0][1] )
        for ptr in xrange(len(parallel_indices)):
            del parallel_indices[ptr][0]
        
        word_list, tag_list, phrase_link = init_context_lists (context_vector, entity_positions, postag_map)
        word_list, tag_list, phrase_link = extract_verb_phrases (word_list, tag_list, phrase_link)
        
        word_list, tag_list, phrase_link = extract_entity_modifiers (word_list, tag_list, phrase_link)
        
        #pos_pattern = extract_pattern (word_list, tag_list, phrase_link)      deleted for now       
        single_tuple_output_dict = prepare_output (word_list, tag_list, phrase_link, entity_positions, pmid_string)
        
        

        
        if low_quality_result( word_list, tag_list, phrase_link, entity_positions ):
            continue
        record_output (word_list, tag_list, phrase_link, single_tuple_output_dict, entity_positions, verb_phrases_score, verb_phrase_output_dict, query_entities, contextual_entity_dict, synonym_dict, cache_synonym_sets )
        
    
    return verb_phrases_score, verb_phrase_output_dict, contextual_entity_dict
        
        
        
        
        
    

def print_to_UI( verb_phrases_score, verb_phrase_output_dict, contextual_entity_dict, start_time, query_fingerprint ):
    
    global cache_suggest_keywords
    global cache_ranked_rps
    
    ranked_rps =[]
    
    output_verb_phrases = sorted(verb_phrases_score.items(), key=lambda x: x[1], reverse = True)
    
    for verb_phrase in output_verb_phrases[:100]:
        
        if " " not in verb_phrase[0] and len(ranked_rps) > 30 and len(verb_phrase_output_dict [ verb_phrase[0] ]) ==1:
            continue
            
        dict_tmp = {}
        
        dict_tmp [ verb_phrase[0] ] = verb_phrase_output_dict [ verb_phrase[0] ]
        ranked_rps.append(dict_tmp)


    
    suggest_keywords=[]
    
    
    output_contextual_entity = sorted(contextual_entity_dict.items(), key=lambda x: x[1], reverse = True)
    
    for contextual_entity in output_contextual_entity[:20]:
        
        suggest_keywords.append( contextual_entity[0] )
      
    
    if len(suggest_keywords)>10:
        suggest_keywords = suggest_keywords[:10]
        
       
    print("--- %s seconds -- return to UI -" % (time.time() - start_time))        
    
    
    cache_suggest_keywords [ query_fingerprint]  = list(suggest_keywords )
    cache_ranked_rps [query_fingerprint] = list (ranked_rps)
    
    return json.dumps({'status':'OK','suggest_keywords':suggest_keywords,'ranked_rps':ranked_rps})




    
@app.route('/search', methods=['GET'])
def search():
    global cache_suggest_keywords
    global cache_ranked_rps
    
    print("Started!")
    start_time = time.time()
    es = Elasticsearch()
    
    postag_map = init_postag_map()
    pattern_score_map = init_pattern_score_map()
    
    query_entities = parse_query(request)
    
    query_fingerprint = " ".join(sorted( query_entities ))
    if query_fingerprint in cache_suggest_keywords:
        return json.dumps({'status':'OK','suggest_keywords':cache_suggest_keywords [query_fingerprint],'ranked_rps':cache_ranked_rps [query_fingerprint]})
        
        
    
    print("--- %s seconds --start reading index !-" % (time.time() - start_time))
    
    parallel_indices = load_context_vectors_lists( es, query_entities )
    
    print("--- %s seconds --finish reading index and start ranking !-" % (time.time() - start_time))
     
    verb_phrases_score, verb_phrase_output_dict, contextual_entity_dict = rank_relations( parallel_indices, postag_map, query_entities )
        
    return print_to_UI( verb_phrases_score, verb_phrase_output_dict, contextual_entity_dict, start_time, query_fingerprint )
    
    
    
    
    

if __name__=="__main__":
    app.run(host='0.0.0.0')
            